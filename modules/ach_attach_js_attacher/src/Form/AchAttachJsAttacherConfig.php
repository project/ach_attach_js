<?php

namespace Drupal\ach_attach_js_attacher\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure ACH Attach JS Attacher.
 */
class AchAttachJsAttacherConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ach_attach_js_attacher_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ach_attach_js_attacher.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['header'] = [
      '#markup' => '<div><p>Configure the paths for which the ACH Attach JS
                    library will be attached. This will call Drupal\'s
                    attachBehaviors function on any content added to the page
                    from Acquia Content Hub.</p></div>',
    ];

    // Create an instance of a Request Path Condition and use its configuration
    // form.
    // Also add the Condition to form_state so it can be used during submission.
    $config = $this->config('ach_attach_js_attacher.settings');
    $request_path_config = $config->get('request_path');
    $condition_manager = \Drupal::service('plugin.manager.condition');
    $request_condition = $condition_manager->createInstance('request_path', isset($request_path_config) ? $request_path_config : []);
    $form_state->set('request_condition', $request_condition);
    $form['request_path'] = $request_condition->buildConfigurationForm([], $form_state);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $condition = $form_state->get(['request_condition']);
    $condition->submitConfigurationForm($form, $form_state);
    $condition_configuration = $condition->getConfiguration();
    $config = $this->config('ach_attach_js_attacher.settings');
    $config->set('request_path', $condition_configuration);
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
