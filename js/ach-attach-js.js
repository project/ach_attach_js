(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.achAttachJs = {
    attach: function (context, settings) {
      $(once('ach-attach-js', 'body')).each(function() {
        Drupal.behaviors.achAttachJs.addLiftEventListeners();
      });
    },
    addLiftEventListeners: function() {
      /**
       * Docs for Lift/Personalization events at https://docs.acquia.com/personalization/api/javascript/events/
       */
      window.addEventListener('acquiaLiftContentAvailable', function(e) {
        var $slot = $('[data-lift-slot="' + e.detail.decision_slot_id + '"]');
        Drupal.attachBehaviors($slot[0], drupalSettings);
      });
    },
  };
})(jQuery, Drupal, drupalSettings, once);
