README.txt
==========

ACH Attach JS is a module that helps to better integrate a Drupal site
with Acquia Lift and Acquia Content Hub.

When Lift adds content to the page (asynchronously) from Content Hub,
your js does not automatically run on the new markup. That's a problem if
the new content has tabs or a drawer or a save flag or any other feature
that requires javascript. This module solves that problem.


The Library
-----------

ACH Attach JS provides a library that calls Drupal.attachBehaviors on content
that is added to the page by Acquia Content Hub.


Attaching the Library
---------------------

There is a sub-module called ACH Attach JS Attacher that provides a
configuration form for attaching that library at the paths of your choice,
just like you can configure at what paths a Block should display.
Alternatively, you could attach the library "manually" in your theme's .info
file, in a preprocess function, etc.
